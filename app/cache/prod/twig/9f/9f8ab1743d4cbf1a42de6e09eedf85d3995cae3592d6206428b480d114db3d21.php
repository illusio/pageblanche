<?php

/* default/accueil.html.twig */
class __TwigTemplate_59a04b5225a1d354b59faf810621115eac589a813ae7d60fc1bee51a469182d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/accueil.html.twig", 1);
        $this->blocks = array(
            'stylecss' => array($this, 'block_stylecss'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylecss($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("stylecss", $context, $blocks);
        echo "
       ";
        // line 4
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "d7cfe60_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d7cfe60_0") : $this->env->getExtension('asset')->getAssetUrl("css/d7cfe60_accueil_1.css");
            // line 6
            echo "\t\t\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
       ";
        } else {
            // asset "d7cfe60"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d7cfe60") : $this->env->getExtension('asset')->getAssetUrl("css/d7cfe60.css");
            echo "\t\t\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
       ";
        }
        unset($context["asset_url"]);
    }

    // line 9
    public function block_javascripts($context, array $blocks = array())
    {
        // line 10
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t<!--<script 
        src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCEu8klAZre34LRG1uUo9iUJW4xDVTxhbU\">
        </script>-->
        
\t
\t";
        // line 16
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "1047cf6_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_1047cf6_0") : $this->env->getExtension('asset')->getAssetUrl("js/1047cf6_accueil_1.js");
            // line 17
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
        } else {
            // asset "1047cf6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_1047cf6") : $this->env->getExtension('asset')->getAssetUrl("js/1047cf6.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
        }
        unset($context["asset_url"]);
        // line 19
        echo "    <script type=\"text/javascript\">

\tGV.launchFunction=init_accueil;

    </script>
";
    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        // line 26
        echo "    accueil
";
    }

    public function getTemplateName()
    {
        return "default/accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 26,  96 => 25,  87 => 19,  73 => 17,  69 => 16,  60 => 10,  57 => 9,  41 => 6,  37 => 4,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylecss %}*/
/* {{ parent() }}*/
/*        {% stylesheets */
/*        		'css/accueil.css' filter='cssrewrite' %}*/
/* 			<link rel="stylesheet" href="{{ asset_url }}" />*/
/*        {% endstylesheets %}*/
/* {% endblock %}*/
/* {% block javascripts %}*/
/* {{ parent() }}*/
/* 	<!--<script */
/*         src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEu8klAZre34LRG1uUo9iUJW4xDVTxhbU">*/
/*         </script>-->*/
/*         */
/* 	*/
/* 	{% javascripts 'js/accueil.js' %}*/
/*         <script src="{{ asset_url }}"></script>*/
/* 	{% endjavascripts %}*/
/*     <script type="text/javascript">*/
/* */
/* 	GV.launchFunction=init_accueil;*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* {% block body %}*/
/*     accueil*/
/* {% endblock %}*/
/* */
/* */
/* */
