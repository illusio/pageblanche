<?php

/* base.html.twig */
class __TwigTemplate_c9281010bcd1536375472454d15dcc47a0f0a7164ec6d821973ccdd09bd11b5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'description' => array($this, 'block_description'),
            'keywords' => array($this, 'block_keywords'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">
        <meta name=\"description\" content=\"";
        // line 7
        $this->displayBlock('description', $context, $blocks);
        echo "\">
        <meta name=\"keywords\" content=\"";
        // line 8
        $this->displayBlock('keywords', $context, $blocks);
        echo "\">
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "        <link rel=\"shortcut icon\" href=\"fav.ico\" /> 
    </head>
    <body>
        ";
        // line 23
        $this->displayBlock('body', $context, $blocks);
        // line 26
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 71
        echo "    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 7
    public function block_description($context, array $blocks = array())
    {
    }

    // line 8
    public function block_keywords($context, array $blocks = array())
    {
    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 10
        echo "             
             ";
        // line 11
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "c02ca66_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_c02ca66_0") : $this->env->getExtension('asset')->getAssetUrl("css/compiled_bootstrap.min_1.css");
            // line 16
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
             ";
            // asset "c02ca66_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_c02ca66_1") : $this->env->getExtension('asset')->getAssetUrl("css/compiled_main_2.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
             ";
            // asset "c02ca66_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_c02ca66_2") : $this->env->getExtension('asset')->getAssetUrl("css/compiled_responsive_3.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
             ";
        } else {
            // asset "c02ca66"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_c02ca66") : $this->env->getExtension('asset')->getAssetUrl("css/compiled.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
             ";
        }
        unset($context["asset_url"]);
        // line 18
        echo "\t\t\t 
        ";
    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        // line 24
        echo "        
        ";
    }

    // line 26
    public function block_javascripts($context, array $blocks = array())
    {
        // line 27
        echo "        
                 <script type=\"text/javascript\">
\t\t\t///////////////////////////////////////////////
    \t\t//définition des différentes variables globales
    \t\t///////////////////////////////////////////////
         \tvar GV={};
         \t
         \t
\t\t\t//indicateur connexion
         \tGV.isConnected;
         \t";
        // line 37
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "user"), "method") == null)) {
            // line 38
            echo "         \t\tGV.isConnected=false;
                ";
        } else {
            // line 40
            echo "\t\t\tGV.isConnected=true;
\t\t";
        }
        // line 42
        echo "
\t\t\t

\t\t//url pour gérer l'environnement de prod quand twig n'est pas pleinement accessible
\t\tGV.urlPrefix;
\t\tGV.mode;

\t\t";
        // line 49
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "environment", array()) == "dev")) {
            // line 50
            echo "                    GV.urlPrefix=\"/app_dev.php/\";
                    GV.mode=\"dev\";
\t\t";
        } else {
            // line 53
            echo "                    GV.urlPrefix=\"/\";
                    GV.mode=\"prod\";
\t\t";
        }
        // line 56
        echo "
\t\t//pseudo variable flash pour passer des infos à des faux popups
\t\tGV.lastFlDatas;

         </script>
         ";
        // line 61
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "cdbba99_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cdbba99_0") : $this->env->getExtension('asset')->getAssetUrl("js/cdbba99_jquery_1.js");
            // line 67
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t ";
            // asset "cdbba99_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cdbba99_1") : $this->env->getExtension('asset')->getAssetUrl("js/cdbba99_bootstrap.min_2.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t ";
            // asset "cdbba99_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cdbba99_2") : $this->env->getExtension('asset')->getAssetUrl("js/cdbba99_tsc_3.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t ";
            // asset "cdbba99_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cdbba99_3") : $this->env->getExtension('asset')->getAssetUrl("js/cdbba99_logic_4.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t ";
        } else {
            // asset "cdbba99"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cdbba99") : $this->env->getExtension('asset')->getAssetUrl("js/cdbba99.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t ";
        }
        unset($context["asset_url"]);
        // line 69
        echo "        
        ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  216 => 69,  184 => 67,  180 => 61,  173 => 56,  168 => 53,  163 => 50,  161 => 49,  152 => 42,  148 => 40,  144 => 38,  142 => 37,  130 => 27,  127 => 26,  122 => 24,  119 => 23,  114 => 18,  88 => 16,  84 => 11,  81 => 10,  78 => 9,  73 => 8,  68 => 7,  62 => 5,  56 => 71,  53 => 26,  51 => 23,  46 => 20,  44 => 9,  40 => 8,  36 => 7,  31 => 5,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         <meta name="viewport" content="width=device-width, user-scalable=no">*/
/*         <meta name="description" content="{% block description %}{% endblock %}">*/
/*         <meta name="keywords" content="{% block keywords %}{% endblock %}">*/
/*         {% block stylesheets %}*/
/*              */
/*              {% stylesheets */
/*                 'bootstrap-3.3.6-dist/css/bootstrap.min.css'*/
/*                 'css/main.css'*/
/*                 'css/responsive.css'  */
/*              	output='css/compiled.css' filter='cssrewrite' %}*/
/*                 <link rel="stylesheet" href="{{ asset_url }}" />*/
/*              {% endstylesheets %}*/
/* 			 */
/*         {% endblock %}*/
/*         <link rel="shortcut icon" href="fav.ico" /> */
/*     </head>*/
/*     <body>*/
/*         {% block body %}*/
/*         */
/*         {% endblock %}*/
/*         {% block javascripts %}*/
/*         */
/*                  <script type="text/javascript">*/
/* 			///////////////////////////////////////////////*/
/*     		//définition des différentes variables globales*/
/*     		///////////////////////////////////////////////*/
/*          	var GV={};*/
/*          	*/
/*          	*/
/* 			//indicateur connexion*/
/*          	GV.isConnected;*/
/*          	{% if app.session.get('user')==null %}*/
/*          		GV.isConnected=false;*/
/*                 {% else %}*/
/* 			GV.isConnected=true;*/
/* 		{% endif %}*/
/* */
/* 			*/
/* */
/* 		//url pour gérer l'environnement de prod quand twig n'est pas pleinement accessible*/
/* 		GV.urlPrefix;*/
/* 		GV.mode;*/
/* */
/* 		{% if app.environment=="dev" %}*/
/*                     GV.urlPrefix="/app_dev.php/";*/
/*                     GV.mode="dev";*/
/* 		{% else %}*/
/*                     GV.urlPrefix="/";*/
/*                     GV.mode="prod";*/
/* 		{% endif %}*/
/* */
/* 		//pseudo variable flash pour passer des infos à des faux popups*/
/* 		GV.lastFlDatas;*/
/* */
/*          </script>*/
/*          {% javascripts*/
/* 		 */
/* 	     'js/jquery.js'*/
/*              'bootstrap-3.3.6-dist/js/bootstrap.min.js'*/
/* 	     'js/tsc.js'*/
/*              'js/logic.js' %}*/
/* 		    <script src="{{ asset_url }}"></script>*/
/* 	 {% endjavascripts %}*/
/*         */
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
