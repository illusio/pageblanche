<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // authentification
        if ($pathinfo === '/authentification') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::authentAction',  '_route' => 'authentification',);
        }

        if (0 === strpos($pathinfo, '/liste_')) {
            // liste_lots
            if (0 === strpos($pathinfo, '/liste_lots') && preg_match('#^/liste_lots/(?P<idprog>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liste_lots')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::lotsAction',));
            }

            // liste_denonces
            if (0 === strpos($pathinfo, '/liste_denonces') && preg_match('#^/liste_denonces/(?P<idprog>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liste_denonces')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::denoncesAction',));
            }

        }

        // import_xml
        if ($pathinfo === '/import_xml') {
            return array (  '_controller' => 'AppBundle\\Controller\\ServicesController::importAction',  '_route' => 'import_xml',);
        }

        // connect_user
        if ($pathinfo === '/connect_user') {
            return array (  '_controller' => 'AppBundle\\Controller\\ServicesController::connectUserAction',  '_route' => 'connect_user',);
        }

        if (0 === strpos($pathinfo, '/soap')) {
            // soapserver
            if ($pathinfo === '/soapserver') {
                return array (  '_controller' => 'AppBundle\\Controller\\SoapController::soapServer0Action',  '_route' => 'soapserver',);
            }

            // soapclient
            if ($pathinfo === '/soapclient') {
                return array (  '_controller' => 'AppBundle\\Controller\\SoapController::soapClient0Action',  '_route' => 'soapclient',);
            }

            if (0 === strpos($pathinfo, '/soap_')) {
                // soap_server2
                if ($pathinfo === '/soap_server2') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapServer2Action',  '_route' => 'soap_server2',);
                }

                // soap_client2
                if ($pathinfo === '/soap_client2') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapClient2Action',  '_route' => 'soap_client2',);
                }

                // soap_server3
                if ($pathinfo === '/soap_server3') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapSever3',  '_route' => 'soap_server3',);
                }

                // soap_client3
                if ($pathinfo === '/soap_client3') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapClient3',  '_route' => 'soap_client3',);
                }

                // soap_server
                if ($pathinfo === '/soap_server') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapServerAction',  '_route' => 'soap_server',);
                }

                // soap_client
                if ($pathinfo === '/soap_client') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapClientAction',  '_route' => 'soap_client',);
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
