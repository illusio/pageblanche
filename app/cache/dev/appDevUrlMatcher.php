<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css/69c5ee8')) {
            // _assetic_69c5ee8
            if ($pathinfo === '/css/69c5ee8.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '69c5ee8',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_69c5ee8',);
            }

            // _assetic_69c5ee8_0
            if ($pathinfo === '/css/69c5ee8_lots_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '69c5ee8',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_69c5ee8_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/d1045b1')) {
            // _assetic_d1045b1
            if ($pathinfo === '/js/d1045b1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'd1045b1',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_d1045b1',);
            }

            // _assetic_d1045b1_0
            if ($pathinfo === '/js/d1045b1_lots_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'd1045b1',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_d1045b1_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/09876a8')) {
            // _assetic_09876a8
            if ($pathinfo === '/css/09876a8.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '09876a8',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_09876a8',);
            }

            // _assetic_09876a8_0
            if ($pathinfo === '/css/09876a8_customauthent_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '09876a8',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_09876a8_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/9010399')) {
            // _assetic_9010399
            if ($pathinfo === '/js/9010399.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 9010399,  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_9010399',);
            }

            // _assetic_9010399_0
            if ($pathinfo === '/js/9010399_custom_client_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 9010399,  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_9010399_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/248c863')) {
            // _assetic_248c863
            if ($pathinfo === '/css/248c863.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '248c863',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_248c863',);
            }

            // _assetic_248c863_0
            if ($pathinfo === '/css/248c863_authentification_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '248c863',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_248c863_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/12b6fd2')) {
            // _assetic_12b6fd2
            if ($pathinfo === '/js/12b6fd2.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '12b6fd2',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_12b6fd2',);
            }

            // _assetic_12b6fd2_0
            if ($pathinfo === '/js/12b6fd2_authent_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '12b6fd2',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_12b6fd2_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/0459909')) {
            // _assetic_0459909
            if ($pathinfo === '/css/0459909.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0459909',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_0459909',);
            }

            // _assetic_0459909_0
            if ($pathinfo === '/css/0459909_denonces_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0459909',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_0459909_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/e2a918b')) {
            // _assetic_e2a918b
            if ($pathinfo === '/js/e2a918b.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'e2a918b',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_e2a918b',);
            }

            // _assetic_e2a918b_0
            if ($pathinfo === '/js/e2a918b_denonces_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'e2a918b',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_e2a918b_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/d7cfe60')) {
            // _assetic_d7cfe60
            if ($pathinfo === '/css/d7cfe60.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'd7cfe60',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_d7cfe60',);
            }

            // _assetic_d7cfe60_0
            if ($pathinfo === '/css/d7cfe60_accueil_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'd7cfe60',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_d7cfe60_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/1047cf6')) {
            // _assetic_1047cf6
            if ($pathinfo === '/js/1047cf6.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '1047cf6',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_1047cf6',);
            }

            // _assetic_1047cf6_0
            if ($pathinfo === '/js/1047cf6_accueil_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '1047cf6',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_1047cf6_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/compiled')) {
            // _assetic_dc98f29
            if ($pathinfo === '/css/compiled.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'dc98f29',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_dc98f29',);
            }

            if (0 === strpos($pathinfo, '/css/compiled_')) {
                // _assetic_dc98f29_0
                if ($pathinfo === '/css/compiled_bootstrap.min_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'dc98f29',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_dc98f29_0',);
                }

                // _assetic_dc98f29_1
                if ($pathinfo === '/css/compiled_main_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'dc98f29',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_dc98f29_1',);
                }

                // _assetic_dc98f29_2
                if ($pathinfo === '/css/compiled_jquery.gritter_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'dc98f29',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_dc98f29_2',);
                }

                // _assetic_dc98f29_3
                if ($pathinfo === '/css/compiled_responsive_4.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'dc98f29',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_dc98f29_3',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/e24dd4d')) {
            // _assetic_e24dd4d
            if ($pathinfo === '/js/e24dd4d.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_e24dd4d',);
            }

            if (0 === strpos($pathinfo, '/js/e24dd4d_')) {
                // _assetic_e24dd4d_0
                if ($pathinfo === '/js/e24dd4d_jquery_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_0',);
                }

                // _assetic_e24dd4d_1
                if ($pathinfo === '/js/e24dd4d_bootstrap.min_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_1',);
                }

                // _assetic_e24dd4d_2
                if ($pathinfo === '/js/e24dd4d_tsc_3.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_2',);
                }

                // _assetic_e24dd4d_3
                if ($pathinfo === '/js/e24dd4d_helpers_4.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_3',);
                }

                // _assetic_e24dd4d_4
                if ($pathinfo === '/js/e24dd4d_jquery.gritter.min_5.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_4',);
                }

                // _assetic_e24dd4d_5
                if ($pathinfo === '/js/e24dd4d_buttonWaiter_6.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_5',);
                }

                // _assetic_e24dd4d_6
                if ($pathinfo === '/js/e24dd4d_simpleWaiter_7.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_6',);
                }

                // _assetic_e24dd4d_7
                if ($pathinfo === '/js/e24dd4d_logic_8.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e24dd4d',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_e24dd4d_7',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // authentification
        if ($pathinfo === '/authentification') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::authentAction',  '_route' => 'authentification',);
        }

        if (0 === strpos($pathinfo, '/liste_')) {
            // liste_lots
            if (0 === strpos($pathinfo, '/liste_lots') && preg_match('#^/liste_lots/(?P<idprog>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liste_lots')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::lotsAction',));
            }

            // liste_denonces
            if (0 === strpos($pathinfo, '/liste_denonces') && preg_match('#^/liste_denonces/(?P<idprog>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liste_denonces')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::denoncesAction',));
            }

        }

        // import_xml
        if ($pathinfo === '/import_xml') {
            return array (  '_controller' => 'AppBundle\\Controller\\ServicesController::importAction',  '_route' => 'import_xml',);
        }

        if (0 === strpos($pathinfo, '/c')) {
            // create_denonce
            if ($pathinfo === '/create_denonce') {
                return array (  '_controller' => 'AppBundle\\Controller\\ServicesController::createDenonceAction',  '_route' => 'create_denonce',);
            }

            // connect_user
            if ($pathinfo === '/connect_user') {
                return array (  '_controller' => 'AppBundle\\Controller\\ServicesController::connectUserAction',  '_route' => 'connect_user',);
            }

        }

        // soapserver
        if ($pathinfo === '/soapserver') {
            return array (  '_controller' => 'AppBundle\\Controller\\SoapController::soapServer0Action',  '_route' => 'soapserver',);
        }

        if (0 === strpos($pathinfo, '/test_')) {
            // test_entrant
            if ($pathinfo === '/test_entrant') {
                return array (  '_controller' => 'AppBundle\\Controller\\SoapController::testOursAction',  '_route' => 'test_entrant',);
            }

            // test_client
            if ($pathinfo === '/test_client') {
                return array (  '_controller' => 'AppBundle\\Controller\\SoapController::testClientAction',  '_route' => 'test_client',);
            }

        }

        // custom_client
        if ($pathinfo === '/custom_client') {
            return array (  '_controller' => 'AppBundle\\Controller\\SoapController::customClientAction',  '_route' => 'custom_client',);
        }

        if (0 === strpos($pathinfo, '/soap')) {
            // soapclient
            if ($pathinfo === '/soapclient') {
                return array (  '_controller' => 'AppBundle\\Controller\\SoapController::soapClient0Action',  '_route' => 'soapclient',);
            }

            if (0 === strpos($pathinfo, '/soap_')) {
                // soap_server2
                if ($pathinfo === '/soap_server2') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapServer2Action',  '_route' => 'soap_server2',);
                }

                // soap_client2
                if ($pathinfo === '/soap_client2') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapClient2Action',  '_route' => 'soap_client2',);
                }

                // soap_server3
                if ($pathinfo === '/soap_server3') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapSever3',  '_route' => 'soap_server3',);
                }

                // soap_client3
                if ($pathinfo === '/soap_client3') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapClient3',  '_route' => 'soap_client3',);
                }

                // soap_server
                if ($pathinfo === '/soap_server') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapServerAction',  '_route' => 'soap_server',);
                }

                // soap_client
                if ($pathinfo === '/soap_client') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SoapTestController::soapClientAction',  '_route' => 'soap_client',);
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
