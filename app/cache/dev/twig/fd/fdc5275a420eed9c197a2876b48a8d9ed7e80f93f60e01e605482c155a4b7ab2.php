<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_30e0391911a31070aefa6b91c0643e0f0c18c36fe0a623b99dd580939dae886e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6a4e7af8f2e6a7b24089a1f5ae4364cac20c6cd1a84237f8c1a87e67976f680 = $this->env->getExtension("native_profiler");
        $__internal_d6a4e7af8f2e6a7b24089a1f5ae4364cac20c6cd1a84237f8c1a87e67976f680->enter($__internal_d6a4e7af8f2e6a7b24089a1f5ae4364cac20c6cd1a84237f8c1a87e67976f680_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d6a4e7af8f2e6a7b24089a1f5ae4364cac20c6cd1a84237f8c1a87e67976f680->leave($__internal_d6a4e7af8f2e6a7b24089a1f5ae4364cac20c6cd1a84237f8c1a87e67976f680_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_538f04819f1a218d0aa61758252641f80d0a42dd1c89b8eaac1af834a9de200d = $this->env->getExtension("native_profiler");
        $__internal_538f04819f1a218d0aa61758252641f80d0a42dd1c89b8eaac1af834a9de200d->enter($__internal_538f04819f1a218d0aa61758252641f80d0a42dd1c89b8eaac1af834a9de200d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_538f04819f1a218d0aa61758252641f80d0a42dd1c89b8eaac1af834a9de200d->leave($__internal_538f04819f1a218d0aa61758252641f80d0a42dd1c89b8eaac1af834a9de200d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_a6ff5460b3e8c911d553417bcd3525ed19d20dd906084c45ab9cfd83a6ead771 = $this->env->getExtension("native_profiler");
        $__internal_a6ff5460b3e8c911d553417bcd3525ed19d20dd906084c45ab9cfd83a6ead771->enter($__internal_a6ff5460b3e8c911d553417bcd3525ed19d20dd906084c45ab9cfd83a6ead771_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_a6ff5460b3e8c911d553417bcd3525ed19d20dd906084c45ab9cfd83a6ead771->leave($__internal_a6ff5460b3e8c911d553417bcd3525ed19d20dd906084c45ab9cfd83a6ead771_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_c9a9e9d97a31d987d2a914438df2a0273cb9c3439455461661e3aa71e65cf415 = $this->env->getExtension("native_profiler");
        $__internal_c9a9e9d97a31d987d2a914438df2a0273cb9c3439455461661e3aa71e65cf415->enter($__internal_c9a9e9d97a31d987d2a914438df2a0273cb9c3439455461661e3aa71e65cf415_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_c9a9e9d97a31d987d2a914438df2a0273cb9c3439455461661e3aa71e65cf415->leave($__internal_c9a9e9d97a31d987d2a914438df2a0273cb9c3439455461661e3aa71e65cf415_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
