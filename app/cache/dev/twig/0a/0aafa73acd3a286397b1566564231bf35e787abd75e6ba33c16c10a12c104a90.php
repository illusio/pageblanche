<?php

/* default/lots.html.twig */
class __TwigTemplate_ccdcc24aa4f048f1d509696531af1c7ee2b9d1aa1644287c1719b665878bb99d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/lots.html.twig", 1);
        $this->blocks = array(
            'stylecss' => array($this, 'block_stylecss'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c83d42b30f3f645f18df7354e0af4310a4884eee90b581417da097e7ffd6a8f = $this->env->getExtension("native_profiler");
        $__internal_8c83d42b30f3f645f18df7354e0af4310a4884eee90b581417da097e7ffd6a8f->enter($__internal_8c83d42b30f3f645f18df7354e0af4310a4884eee90b581417da097e7ffd6a8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/lots.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8c83d42b30f3f645f18df7354e0af4310a4884eee90b581417da097e7ffd6a8f->leave($__internal_8c83d42b30f3f645f18df7354e0af4310a4884eee90b581417da097e7ffd6a8f_prof);

    }

    // line 2
    public function block_stylecss($context, array $blocks = array())
    {
        $__internal_b68c91844cc15a4994e77db3f814ce6feafc1472df91852cb8b0bfe036e53702 = $this->env->getExtension("native_profiler");
        $__internal_b68c91844cc15a4994e77db3f814ce6feafc1472df91852cb8b0bfe036e53702->enter($__internal_b68c91844cc15a4994e77db3f814ce6feafc1472df91852cb8b0bfe036e53702_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylecss"));

        // line 3
        $this->displayParentBlock("stylecss", $context, $blocks);
        echo "
       ";
        // line 4
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "69c5ee8_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_69c5ee8_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/69c5ee8_lots_1.css");
            // line 6
            echo "\t\t\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
       ";
        } else {
            // asset "69c5ee8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_69c5ee8") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/69c5ee8.css");
            echo "\t\t\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
       ";
        }
        unset($context["asset_url"]);
        
        $__internal_b68c91844cc15a4994e77db3f814ce6feafc1472df91852cb8b0bfe036e53702->leave($__internal_b68c91844cc15a4994e77db3f814ce6feafc1472df91852cb8b0bfe036e53702_prof);

    }

    // line 9
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_0e28b472d83ebf32e0c10af3e7f059254bd9c8d927d84210d7a63ac58adfdf43 = $this->env->getExtension("native_profiler");
        $__internal_0e28b472d83ebf32e0c10af3e7f059254bd9c8d927d84210d7a63ac58adfdf43->enter($__internal_0e28b472d83ebf32e0c10af3e7f059254bd9c8d927d84210d7a63ac58adfdf43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 10
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t<!--<script 
        src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCEu8klAZre34LRG1uUo9iUJW4xDVTxhbU\">
        </script>-->
        
\t
\t";
        // line 16
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "d1045b1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d1045b1_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/d1045b1_lots_1.js");
            // line 17
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
        } else {
            // asset "d1045b1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d1045b1") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/d1045b1.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
        }
        unset($context["asset_url"]);
        // line 19
        echo "    <script type=\"text/javascript\">

\tGV.launchFunction=init_lots;

    </script>
";
        
        $__internal_0e28b472d83ebf32e0c10af3e7f059254bd9c8d927d84210d7a63ac58adfdf43->leave($__internal_0e28b472d83ebf32e0c10af3e7f059254bd9c8d927d84210d7a63ac58adfdf43_prof);

    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        $__internal_f859cce7c5b10fe29fe764c02707b67f1c90dddea650db06724ca391baeb58f3 = $this->env->getExtension("native_profiler");
        $__internal_f859cce7c5b10fe29fe764c02707b67f1c90dddea650db06724ca391baeb58f3->enter($__internal_f859cce7c5b10fe29fe764c02707b67f1c90dddea650db06724ca391baeb58f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 26
        echo "    <div class=\"mAuto widthStandard relative\">
        <h3 class=\"tac\">Lots du programme ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["prog"]) ? $context["prog"] : $this->getContext($context, "prog")), "prog_nom", array()), "html", null, true);
        echo "</h3><br/>
        
        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lots"]) ? $context["lots"] : $this->getContext($context, "lots")));
        foreach ($context['_seq'] as $context["_key"] => $context["lot"]) {
            // line 30
            echo "            <div class=\"panel panel-default relative\">
                <div class=\"panelHeadingAction\">


                </div>
                <div class=\"panel-heading\">
                  <h3 class=\"panel-title\" >";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_numero", array()), "html", null, true);
            echo "   </h3>
                </div>
                <div class=\"panel-body relative\">
                    <p>
                        <span class=\"bold\"> Référence :</span> ";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_ref", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Type :</span> ";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_type_bien", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Orientation :</span> ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_orientation", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Surface habitable :</span> ";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_surface_habitable", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Nombre de pièces :</span> ";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_nb_piece", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Etage :</span> ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_etage", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Date de livraison :</span> ";
            // line 46
            echo $this->getAttribute($context["lot"], "livraisonFormat", array());
            echo "<br/>
                        <span class=\"bold\"> Prix :</span> ";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_prix_accession", array()), "html", null, true);
            echo " €<br/>
                        <span class=\"bold\"> Loyer :</span> ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_loyer", array()), "html", null, true);
            echo " €<br/>
                        <span class=\"bold\"> Tva réduite :</span> ";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "tvaRed", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Optionné :</span> ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "isOption", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Taux tva :</span> ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_taux_tva", array()), "html", null, true);
            echo " %<br/>
                        ";
            // line 52
            if (($this->getAttribute($context["lot"], "lot_option_fin", array()) > 0)) {
                // line 53
                echo "                        <span class=\"bold\"> Fin option :</span> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["lot"], "lot_option_fin", array()), "d/m/Y"), "html", null, true);
                echo "<br/>
                        ";
            }
            // line 55
            echo "                        <span class=\"bold\"> Nombre parkings :</span> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_nb_parkings", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Nombre garages  :</span> ";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_nb_garages", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Tranche :</span> ";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_num_tranche", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Batiment :</span> ";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["lot"], "lot_nom_batiment", array()), "html", null, true);
            echo "<br/>
                    </p>
                </div>
            
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lot'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "    </div>
";
        
        $__internal_f859cce7c5b10fe29fe764c02707b67f1c90dddea650db06724ca391baeb58f3->leave($__internal_f859cce7c5b10fe29fe764c02707b67f1c90dddea650db06724ca391baeb58f3_prof);

    }

    public function getTemplateName()
    {
        return "default/lots.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 64,  216 => 58,  212 => 57,  208 => 56,  203 => 55,  197 => 53,  195 => 52,  191 => 51,  187 => 50,  183 => 49,  179 => 48,  175 => 47,  171 => 46,  167 => 45,  163 => 44,  159 => 43,  155 => 42,  151 => 41,  147 => 40,  140 => 36,  132 => 30,  128 => 29,  123 => 27,  120 => 26,  114 => 25,  102 => 19,  88 => 17,  84 => 16,  75 => 10,  69 => 9,  50 => 6,  46 => 4,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylecss %}*/
/* {{ parent() }}*/
/*        {% stylesheets */
/*        		'css/lots.css' filter='cssrewrite' %}*/
/* 			<link rel="stylesheet" href="{{ asset_url }}" />*/
/*        {% endstylesheets %}*/
/* {% endblock %}*/
/* {% block javascripts %}*/
/* {{ parent() }}*/
/* 	<!--<script */
/*         src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEu8klAZre34LRG1uUo9iUJW4xDVTxhbU">*/
/*         </script>-->*/
/*         */
/* 	*/
/* 	{% javascripts 'js/lots.js' %}*/
/*         <script src="{{ asset_url }}"></script>*/
/* 	{% endjavascripts %}*/
/*     <script type="text/javascript">*/
/* */
/* 	GV.launchFunction=init_lots;*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* {% block body %}*/
/*     <div class="mAuto widthStandard relative">*/
/*         <h3 class="tac">Lots du programme {{ prog.prog_nom }}</h3><br/>*/
/*         */
/*         {% for lot in lots %}*/
/*             <div class="panel panel-default relative">*/
/*                 <div class="panelHeadingAction">*/
/* */
/* */
/*                 </div>*/
/*                 <div class="panel-heading">*/
/*                   <h3 class="panel-title" >{{ lot.lot_numero }}   </h3>*/
/*                 </div>*/
/*                 <div class="panel-body relative">*/
/*                     <p>*/
/*                         <span class="bold"> Référence :</span> {{ lot.lot_ref }}<br/>*/
/*                         <span class="bold"> Type :</span> {{ lot.lot_type_bien }}<br/>*/
/*                         <span class="bold"> Orientation :</span> {{ lot.lot_orientation}}<br/>*/
/*                         <span class="bold"> Surface habitable :</span> {{ lot.lot_surface_habitable }}<br/>*/
/*                         <span class="bold"> Nombre de pièces :</span> {{ lot.lot_nb_piece }}<br/>*/
/*                         <span class="bold"> Etage :</span> {{ lot.lot_etage }}<br/>*/
/*                         <span class="bold"> Date de livraison :</span> {{ lot.livraisonFormat|raw }}<br/>*/
/*                         <span class="bold"> Prix :</span> {{ lot.lot_prix_accession }} €<br/>*/
/*                         <span class="bold"> Loyer :</span> {{ lot.lot_loyer }} €<br/>*/
/*                         <span class="bold"> Tva réduite :</span> {{ lot.tvaRed }}<br/>*/
/*                         <span class="bold"> Optionné :</span> {{ lot.isOption }}<br/>*/
/*                         <span class="bold"> Taux tva :</span> {{ lot.lot_taux_tva }} %<br/>*/
/*                         {% if lot.lot_option_fin>0 %}*/
/*                         <span class="bold"> Fin option :</span> {{ lot.lot_option_fin|date('d/m/Y') }}<br/>*/
/*                         {% endif %}*/
/*                         <span class="bold"> Nombre parkings :</span> {{ lot.lot_nb_parkings }}<br/>*/
/*                         <span class="bold"> Nombre garages  :</span> {{ lot.lot_nb_garages }}<br/>*/
/*                         <span class="bold"> Tranche :</span> {{ lot.lot_num_tranche }}<br/>*/
/*                         <span class="bold"> Batiment :</span> {{ lot.lot_nom_batiment }}<br/>*/
/*                     </p>*/
/*                 </div>*/
/*             */
/*             </div>*/
/*         {% endfor %}*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* */
/* */
