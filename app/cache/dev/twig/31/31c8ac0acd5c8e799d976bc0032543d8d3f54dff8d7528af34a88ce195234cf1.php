<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_812549263e4ea9df15cc274e707d5392d6d44ae0f36083ebb8cd438088436802 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_251d7f0e993d8d19615f4f50fb7ed90473078e20d33806d21f5f7ae61fa0f794 = $this->env->getExtension("native_profiler");
        $__internal_251d7f0e993d8d19615f4f50fb7ed90473078e20d33806d21f5f7ae61fa0f794->enter($__internal_251d7f0e993d8d19615f4f50fb7ed90473078e20d33806d21f5f7ae61fa0f794_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_251d7f0e993d8d19615f4f50fb7ed90473078e20d33806d21f5f7ae61fa0f794->leave($__internal_251d7f0e993d8d19615f4f50fb7ed90473078e20d33806d21f5f7ae61fa0f794_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_8216ad6a94a87bf0afc8394da3bbfe111b89f0deb4b0d7a0c01edee75b23cadc = $this->env->getExtension("native_profiler");
        $__internal_8216ad6a94a87bf0afc8394da3bbfe111b89f0deb4b0d7a0c01edee75b23cadc->enter($__internal_8216ad6a94a87bf0afc8394da3bbfe111b89f0deb4b0d7a0c01edee75b23cadc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_8216ad6a94a87bf0afc8394da3bbfe111b89f0deb4b0d7a0c01edee75b23cadc->leave($__internal_8216ad6a94a87bf0afc8394da3bbfe111b89f0deb4b0d7a0c01edee75b23cadc_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0a3e19c62e03ab356bfe85f80f665331ed2947c05191832d38f2284ff61da5d7 = $this->env->getExtension("native_profiler");
        $__internal_0a3e19c62e03ab356bfe85f80f665331ed2947c05191832d38f2284ff61da5d7->enter($__internal_0a3e19c62e03ab356bfe85f80f665331ed2947c05191832d38f2284ff61da5d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_0a3e19c62e03ab356bfe85f80f665331ed2947c05191832d38f2284ff61da5d7->leave($__internal_0a3e19c62e03ab356bfe85f80f665331ed2947c05191832d38f2284ff61da5d7_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_27cba25206d2fecae15fa5e73733f9f5a58234af4dfc291d4547e451c24adb04 = $this->env->getExtension("native_profiler");
        $__internal_27cba25206d2fecae15fa5e73733f9f5a58234af4dfc291d4547e451c24adb04->enter($__internal_27cba25206d2fecae15fa5e73733f9f5a58234af4dfc291d4547e451c24adb04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_27cba25206d2fecae15fa5e73733f9f5a58234af4dfc291d4547e451c24adb04->leave($__internal_27cba25206d2fecae15fa5e73733f9f5a58234af4dfc291d4547e451c24adb04_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
