<?php

/* default/accueil.html.twig */
class __TwigTemplate_96cb403c8abb13121218923bdc8b7e9c99b649cdee93ba09a797020de67a493e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/accueil.html.twig", 1);
        $this->blocks = array(
            'stylecss' => array($this, 'block_stylecss'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_233c197ccb2436e86ea45f82215b76515cf7d729a8480f9897a3e4b580eed4b7 = $this->env->getExtension("native_profiler");
        $__internal_233c197ccb2436e86ea45f82215b76515cf7d729a8480f9897a3e4b580eed4b7->enter($__internal_233c197ccb2436e86ea45f82215b76515cf7d729a8480f9897a3e4b580eed4b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/accueil.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_233c197ccb2436e86ea45f82215b76515cf7d729a8480f9897a3e4b580eed4b7->leave($__internal_233c197ccb2436e86ea45f82215b76515cf7d729a8480f9897a3e4b580eed4b7_prof);

    }

    // line 2
    public function block_stylecss($context, array $blocks = array())
    {
        $__internal_d051d7fada27606851472393f6721fe30a4145f2b22f2170af7e6f0e058ad422 = $this->env->getExtension("native_profiler");
        $__internal_d051d7fada27606851472393f6721fe30a4145f2b22f2170af7e6f0e058ad422->enter($__internal_d051d7fada27606851472393f6721fe30a4145f2b22f2170af7e6f0e058ad422_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylecss"));

        // line 3
        $this->displayParentBlock("stylecss", $context, $blocks);
        echo "
       ";
        // line 4
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "d7cfe60_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d7cfe60_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/d7cfe60_accueil_1.css");
            // line 6
            echo "\t\t\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
       ";
        } else {
            // asset "d7cfe60"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d7cfe60") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/d7cfe60.css");
            echo "\t\t\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
       ";
        }
        unset($context["asset_url"]);
        
        $__internal_d051d7fada27606851472393f6721fe30a4145f2b22f2170af7e6f0e058ad422->leave($__internal_d051d7fada27606851472393f6721fe30a4145f2b22f2170af7e6f0e058ad422_prof);

    }

    // line 9
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_eb60a4a84c8ea6440a7d27eed3a739b0c88521363bb98d826d6ae6e547ac3157 = $this->env->getExtension("native_profiler");
        $__internal_eb60a4a84c8ea6440a7d27eed3a739b0c88521363bb98d826d6ae6e547ac3157->enter($__internal_eb60a4a84c8ea6440a7d27eed3a739b0c88521363bb98d826d6ae6e547ac3157_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 10
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t<!--<script 
        src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCEu8klAZre34LRG1uUo9iUJW4xDVTxhbU\">
        </script>-->
        
\t
\t";
        // line 16
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "1047cf6_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_1047cf6_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/1047cf6_accueil_1.js");
            // line 17
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
        } else {
            // asset "1047cf6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_1047cf6") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/1047cf6.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
        }
        unset($context["asset_url"]);
        // line 19
        echo "    <script type=\"text/javascript\">

\tGV.launchFunction=init_accueil;

    </script>
";
        
        $__internal_eb60a4a84c8ea6440a7d27eed3a739b0c88521363bb98d826d6ae6e547ac3157->leave($__internal_eb60a4a84c8ea6440a7d27eed3a739b0c88521363bb98d826d6ae6e547ac3157_prof);

    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        $__internal_4cc0469cc3a899be3bd6775362dbfb614665d2e50187676a723a83f472d03a38 = $this->env->getExtension("native_profiler");
        $__internal_4cc0469cc3a899be3bd6775362dbfb614665d2e50187676a723a83f472d03a38->enter($__internal_4cc0469cc3a899be3bd6775362dbfb614665d2e50187676a723a83f472d03a38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 26
        echo "    <div class=\"mAuto widthStandard relative\">
        <h3 class=\"tac\">Liste des programmes</h3>
        
        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["programmes"]) ? $context["programmes"] : $this->getContext($context, "programmes")));
        foreach ($context['_seq'] as $context["_key"] => $context["prog"]) {
            // line 30
            echo "            <div class=\"panel panel-default relative\">
                <div class=\"panelHeadingAction\">


                </div>
                <div class=\"panel-heading\">
                  <h3 class=\"panel-title\" >";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_nom", array()), "html", null, true);
            echo "   </h3>
                </div>
                <div class=\"panel-body relative\">
                    <p>
                        
                        <span class=\"bold\"> Référence :</span> ";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_ref", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Numéro :</span> ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_numero", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Adresse :</span> ";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_adresse", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Code postal :</span> ";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_cp", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Ville :</span> ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_ville", array()), "html", null, true);
            echo "<br/>
                       
                        <span class=\"bold\"> Mail :</span> ";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_mail", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Illustration :</span> <br/> <img src=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_image_url", array()), "html", null, true);
            echo "\" width=\"400px\" height=\"auto\"/><br/>
                        <span class=\"bold\"> Lien :</span> <a target=\"_blank\" href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_url", array()), "html", null, true);
            echo "\">Voir</a><br/>
                        <span class=\"bold\"> Plaquette :</span> <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_plaquette_url", array()), "html", null, true);
            echo "\">Voir</a><br/>
                        <span class=\"bold\"> Nombre de lots :</span> ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_nb_lots", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> Date de  livraison :</span> ";
            // line 52
            echo $this->getAttribute($context["prog"], "livraisonFormat", array());
            echo "<br/>
                        <span class=\"bold\"> HQE :</span> ";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "hqe", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> BBC :</span> ";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "bbc", array()), "html", null, true);
            echo "<br/>
                        <span class=\"bold\"> descriptif court :</span> ";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "prog_descriptif_cours", array()), "html", null, true);
            echo "<br/>

                        <a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("liste_lots", array("idprog" => $this->getAttribute($context["prog"], "idprogrammes", array()))), "html", null, true);
            echo "\"><button id=\"voirLots_";
            echo twig_escape_filter($this->env, $this->getAttribute($context["prog"], "idprogrammes", array()), "html", null, true);
            echo "\" type=\"button\" class=\"voirLots btn btn-default\">Voir les lots</button></a>

                    </p>
                </div>
            
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prog'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "    </div>
";
        
        $__internal_4cc0469cc3a899be3bd6775362dbfb614665d2e50187676a723a83f472d03a38->leave($__internal_4cc0469cc3a899be3bd6775362dbfb614665d2e50187676a723a83f472d03a38_prof);

    }

    public function getTemplateName()
    {
        return "default/accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 64,  203 => 57,  198 => 55,  194 => 54,  190 => 53,  186 => 52,  182 => 51,  178 => 50,  174 => 49,  170 => 48,  166 => 47,  161 => 45,  157 => 44,  153 => 43,  149 => 42,  145 => 41,  137 => 36,  129 => 30,  125 => 29,  120 => 26,  114 => 25,  102 => 19,  88 => 17,  84 => 16,  75 => 10,  69 => 9,  50 => 6,  46 => 4,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylecss %}*/
/* {{ parent() }}*/
/*        {% stylesheets */
/*        		'css/accueil.css' filter='cssrewrite' %}*/
/* 			<link rel="stylesheet" href="{{ asset_url }}" />*/
/*        {% endstylesheets %}*/
/* {% endblock %}*/
/* {% block javascripts %}*/
/* {{ parent() }}*/
/* 	<!--<script */
/*         src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEu8klAZre34LRG1uUo9iUJW4xDVTxhbU">*/
/*         </script>-->*/
/*         */
/* 	*/
/* 	{% javascripts 'js/accueil.js' %}*/
/*         <script src="{{ asset_url }}"></script>*/
/* 	{% endjavascripts %}*/
/*     <script type="text/javascript">*/
/* */
/* 	GV.launchFunction=init_accueil;*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* {% block body %}*/
/*     <div class="mAuto widthStandard relative">*/
/*         <h3 class="tac">Liste des programmes</h3>*/
/*         */
/*         {% for prog in programmes %}*/
/*             <div class="panel panel-default relative">*/
/*                 <div class="panelHeadingAction">*/
/* */
/* */
/*                 </div>*/
/*                 <div class="panel-heading">*/
/*                   <h3 class="panel-title" >{{ prog.prog_nom }}   </h3>*/
/*                 </div>*/
/*                 <div class="panel-body relative">*/
/*                     <p>*/
/*                         */
/*                         <span class="bold"> Référence :</span> {{ prog.prog_ref }}<br/>*/
/*                         <span class="bold"> Numéro :</span> {{ prog.prog_numero }}<br/>*/
/*                         <span class="bold"> Adresse :</span> {{ prog.prog_adresse }}<br/>*/
/*                         <span class="bold"> Code postal :</span> {{ prog.prog_cp }}<br/>*/
/*                         <span class="bold"> Ville :</span> {{ prog.prog_ville }}<br/>*/
/*                        */
/*                         <span class="bold"> Mail :</span> {{ prog.prog_mail }}<br/>*/
/*                         <span class="bold"> Illustration :</span> <br/> <img src="{{ prog.prog_image_url }}" width="400px" height="auto"/><br/>*/
/*                         <span class="bold"> Lien :</span> <a target="_blank" href="{{ prog.prog_url }}">Voir</a><br/>*/
/*                         <span class="bold"> Plaquette :</span> <a href="{{ prog.prog_plaquette_url }}">Voir</a><br/>*/
/*                         <span class="bold"> Nombre de lots :</span> {{ prog.prog_nb_lots }}<br/>*/
/*                         <span class="bold"> Date de  livraison :</span> {{ prog.livraisonFormat|raw }}<br/>*/
/*                         <span class="bold"> HQE :</span> {{ prog.hqe }}<br/>*/
/*                         <span class="bold"> BBC :</span> {{ prog.bbc }}<br/>*/
/*                         <span class="bold"> descriptif court :</span> {{ prog.prog_descriptif_cours }}<br/>*/
/* */
/*                         <a href="{{ path('liste_lots',{idprog:prog.idprogrammes}) }}"><button id="voirLots_{{ prog.idprogrammes }}" type="button" class="voirLots btn btn-default">Voir les lots</button></a>*/
/* */
/*                     </p>*/
/*                 </div>*/
/*             */
/*             </div>*/
/*         {% endfor %}*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* */
/* */
