<?php

/* base.html.twig */
class __TwigTemplate_c28f51891d7d1d1fbdf44ec7c58ac591b760dd2611534c004bcb180c4f26a1ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'description' => array($this, 'block_description'),
            'keywords' => array($this, 'block_keywords'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e1c5699ff58ca2e56df0506bae920b877310dc7630243b36939964309030e1c = $this->env->getExtension("native_profiler");
        $__internal_0e1c5699ff58ca2e56df0506bae920b877310dc7630243b36939964309030e1c->enter($__internal_0e1c5699ff58ca2e56df0506bae920b877310dc7630243b36939964309030e1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">
        <meta name=\"description\" content=\"";
        // line 7
        $this->displayBlock('description', $context, $blocks);
        echo "\">
        <meta name=\"keywords\" content=\"";
        // line 8
        $this->displayBlock('keywords', $context, $blocks);
        echo "\">
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "        <link rel=\"shortcut icon\" href=\"fav.ico\" /> 
    </head>
    <body>
        ";
        // line 24
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "user"), "method") != null)) {
            // line 25
            echo "            <nav class=\"navbar navbar-inverse \">
                <div class=\"container\">
                  <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                      <span class=\"sr-only\">Toggle navigation</span>
                      <span class=\"icon-bar\"></span>
                      <span class=\"icon-bar\"></span>
                      <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">Page blanche</a>
                  </div>
                  <div id=\"navbar\" class=\"collapse navbar-collapse\">
                    <ul class=\"nav navbar-nav\">
                      <li class=\"active\"><a href=\"#\">Données xml</a></li>
                      
                    </ul>
                      
                    <ul class=\"nav navbar-nav navbar-right\">
                        ";
            // line 43
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "user"), "method") != null)) {
                // line 44
                echo "                        <a class=\"navbar-brand\" >Bonjour ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["userInfos"]) ? $context["userInfos"] : $this->getContext($context, "userInfos")), "user_prenom", array()), "html", null, true);
                echo "</a>
                        ";
            } else {
                // line 46
                echo "                        
                        ";
            }
            // line 48
            echo "                    </ul>
                  </div><!--/.nav-collapse -->
                </div>
            </nav>
        ";
        } else {
            // line 53
            echo "\t\t\t
\t";
        }
        // line 55
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 58
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 107
        echo "    </body>
</html>
";
        
        $__internal_0e1c5699ff58ca2e56df0506bae920b877310dc7630243b36939964309030e1c->leave($__internal_0e1c5699ff58ca2e56df0506bae920b877310dc7630243b36939964309030e1c_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_3a453dea25e0f4ca6c479e85ae73c2fc005ede38ca4e88518472e573cc874419 = $this->env->getExtension("native_profiler");
        $__internal_3a453dea25e0f4ca6c479e85ae73c2fc005ede38ca4e88518472e573cc874419->enter($__internal_3a453dea25e0f4ca6c479e85ae73c2fc005ede38ca4e88518472e573cc874419_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_3a453dea25e0f4ca6c479e85ae73c2fc005ede38ca4e88518472e573cc874419->leave($__internal_3a453dea25e0f4ca6c479e85ae73c2fc005ede38ca4e88518472e573cc874419_prof);

    }

    // line 7
    public function block_description($context, array $blocks = array())
    {
        $__internal_2a0fc015120ed6fafd1dc77a30cbfef2373cb36d67677db73f0ef20e53c49c40 = $this->env->getExtension("native_profiler");
        $__internal_2a0fc015120ed6fafd1dc77a30cbfef2373cb36d67677db73f0ef20e53c49c40->enter($__internal_2a0fc015120ed6fafd1dc77a30cbfef2373cb36d67677db73f0ef20e53c49c40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        
        $__internal_2a0fc015120ed6fafd1dc77a30cbfef2373cb36d67677db73f0ef20e53c49c40->leave($__internal_2a0fc015120ed6fafd1dc77a30cbfef2373cb36d67677db73f0ef20e53c49c40_prof);

    }

    // line 8
    public function block_keywords($context, array $blocks = array())
    {
        $__internal_211bc9c0de4a482690de7aa7ea69b25b86c6b3126b7d95e8786ffeff6b195776 = $this->env->getExtension("native_profiler");
        $__internal_211bc9c0de4a482690de7aa7ea69b25b86c6b3126b7d95e8786ffeff6b195776->enter($__internal_211bc9c0de4a482690de7aa7ea69b25b86c6b3126b7d95e8786ffeff6b195776_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "keywords"));

        
        $__internal_211bc9c0de4a482690de7aa7ea69b25b86c6b3126b7d95e8786ffeff6b195776->leave($__internal_211bc9c0de4a482690de7aa7ea69b25b86c6b3126b7d95e8786ffeff6b195776_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b0f4d98a3081ed0b1e30fa82a9279f866cb1ad9579c98c2e743dbcf0734d3364 = $this->env->getExtension("native_profiler");
        $__internal_b0f4d98a3081ed0b1e30fa82a9279f866cb1ad9579c98c2e743dbcf0734d3364->enter($__internal_b0f4d98a3081ed0b1e30fa82a9279f866cb1ad9579c98c2e743dbcf0734d3364_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "             
             ";
        // line 11
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "dc98f29_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_dc98f29_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/compiled_bootstrap.min_1.css");
            // line 17
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
             ";
            // asset "dc98f29_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_dc98f29_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/compiled_main_2.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
             ";
            // asset "dc98f29_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_dc98f29_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/compiled_jquery.gritter_3.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
             ";
            // asset "dc98f29_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_dc98f29_3") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/compiled_responsive_4.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
             ";
        } else {
            // asset "dc98f29"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_dc98f29") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/compiled.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
             ";
        }
        unset($context["asset_url"]);
        // line 19
        echo "\t\t\t 
        ";
        
        $__internal_b0f4d98a3081ed0b1e30fa82a9279f866cb1ad9579c98c2e743dbcf0734d3364->leave($__internal_b0f4d98a3081ed0b1e30fa82a9279f866cb1ad9579c98c2e743dbcf0734d3364_prof);

    }

    // line 55
    public function block_body($context, array $blocks = array())
    {
        $__internal_56ea44a335e79c97fd265b695f12755e9c7427bfe5451f5787e6ffe910acf36f = $this->env->getExtension("native_profiler");
        $__internal_56ea44a335e79c97fd265b695f12755e9c7427bfe5451f5787e6ffe910acf36f->enter($__internal_56ea44a335e79c97fd265b695f12755e9c7427bfe5451f5787e6ffe910acf36f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 56
        echo "        
        ";
        
        $__internal_56ea44a335e79c97fd265b695f12755e9c7427bfe5451f5787e6ffe910acf36f->leave($__internal_56ea44a335e79c97fd265b695f12755e9c7427bfe5451f5787e6ffe910acf36f_prof);

    }

    // line 58
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1534b7f18949ed8d7bccb02fdf38c1e285437bf687c22c893a0ec76bed745f4a = $this->env->getExtension("native_profiler");
        $__internal_1534b7f18949ed8d7bccb02fdf38c1e285437bf687c22c893a0ec76bed745f4a->enter($__internal_1534b7f18949ed8d7bccb02fdf38c1e285437bf687c22c893a0ec76bed745f4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 59
        echo "        
                 <script type=\"text/javascript\">
\t\t\t///////////////////////////////////////////////
    \t\t//définition des différentes variables globales
    \t\t///////////////////////////////////////////////
         \tvar GV={};
         \t
         \t
\t\t\t//indicateur connexion
         \tGV.isConnected;
         \t";
        // line 69
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "user"), "method") == null)) {
            // line 70
            echo "         \t\tGV.isConnected=false;
                ";
        } else {
            // line 72
            echo "\t\t\tGV.isConnected=true;
\t\t";
        }
        // line 74
        echo "
\t\t\t

\t\t//url pour gérer l'environnement de prod quand twig n'est pas pleinement accessible
\t\tGV.urlPrefix;
\t\tGV.mode;

\t\t";
        // line 81
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "environment", array()) == "dev")) {
            // line 82
            echo "                    GV.urlPrefix=\"/app_dev.php/\";
                    GV.mode=\"dev\";
\t\t";
        } else {
            // line 85
            echo "                    GV.urlPrefix=\"/\";
                    GV.mode=\"prod\";
\t\t";
        }
        // line 88
        echo "
\t\t//pseudo variable flash pour passer des infos à des faux popups
\t\tGV.lastFlDatas;

         </script>
         ";
        // line 93
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "e24dd4d_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_jquery_1.js");
            // line 103
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
            // asset "e24dd4d_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_bootstrap.min_2.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
            // asset "e24dd4d_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_tsc_3.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
            // asset "e24dd4d_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_3") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_helpers_4.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
            // asset "e24dd4d_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_4") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_jquery.gritter.min_5.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
            // asset "e24dd4d_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_5") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_buttonWaiter_6.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
            // asset "e24dd4d_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_6") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_simpleWaiter_7.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
            // asset "e24dd4d_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d_7") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d_logic_8.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
        } else {
            // asset "e24dd4d"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e24dd4d") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/e24dd4d.js");
            echo "\t\t    <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t ";
        }
        unset($context["asset_url"]);
        // line 105
        echo "        
        ";
        
        $__internal_1534b7f18949ed8d7bccb02fdf38c1e285437bf687c22c893a0ec76bed745f4a->leave($__internal_1534b7f18949ed8d7bccb02fdf38c1e285437bf687c22c893a0ec76bed745f4a_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  331 => 105,  275 => 103,  271 => 93,  264 => 88,  259 => 85,  254 => 82,  252 => 81,  243 => 74,  239 => 72,  235 => 70,  233 => 69,  221 => 59,  215 => 58,  207 => 56,  201 => 55,  193 => 19,  161 => 17,  157 => 11,  154 => 10,  148 => 9,  137 => 8,  126 => 7,  114 => 5,  105 => 107,  102 => 58,  99 => 55,  95 => 53,  88 => 48,  84 => 46,  78 => 44,  76 => 43,  56 => 25,  54 => 24,  49 => 21,  47 => 9,  43 => 8,  39 => 7,  34 => 5,  28 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         <meta name="viewport" content="width=device-width, user-scalable=no">*/
/*         <meta name="description" content="{% block description %}{% endblock %}">*/
/*         <meta name="keywords" content="{% block keywords %}{% endblock %}">*/
/*         {% block stylesheets %}*/
/*              */
/*              {% stylesheets */
/*                 'bootstrap-3.3.6-dist/css/bootstrap.min.css'*/
/*                 'css/main.css'*/
/*                 'libs/gritter/css/jquery.gritter.css'*/
/*                 'css/responsive.css'  */
/*              	output='css/compiled.css' filter='cssrewrite' %}*/
/*                 <link rel="stylesheet" href="{{ asset_url }}" />*/
/*              {% endstylesheets %}*/
/* 			 */
/*         {% endblock %}*/
/*         <link rel="shortcut icon" href="fav.ico" /> */
/*     </head>*/
/*     <body>*/
/*         {% if app.session.get('user')!=null %}*/
/*             <nav class="navbar navbar-inverse ">*/
/*                 <div class="container">*/
/*                   <div class="navbar-header">*/
/*                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">*/
/*                       <span class="sr-only">Toggle navigation</span>*/
/*                       <span class="icon-bar"></span>*/
/*                       <span class="icon-bar"></span>*/
/*                       <span class="icon-bar"></span>*/
/*                     </button>*/
/*                     <a class="navbar-brand" href="#">Page blanche</a>*/
/*                   </div>*/
/*                   <div id="navbar" class="collapse navbar-collapse">*/
/*                     <ul class="nav navbar-nav">*/
/*                       <li class="active"><a href="#">Données xml</a></li>*/
/*                       */
/*                     </ul>*/
/*                       */
/*                     <ul class="nav navbar-nav navbar-right">*/
/*                         {% if app.session.get('user')!=null %}*/
/*                         <a class="navbar-brand" >Bonjour {{ userInfos.user_prenom }}</a>*/
/*                         {% else %}*/
/*                         */
/*                         {% endif %}*/
/*                     </ul>*/
/*                   </div><!--/.nav-collapse -->*/
/*                 </div>*/
/*             </nav>*/
/*         {% else %}*/
/* 			*/
/* 	{% endif %}*/
/*         {% block body %}*/
/*         */
/*         {% endblock %}*/
/*         {% block javascripts %}*/
/*         */
/*                  <script type="text/javascript">*/
/* 			///////////////////////////////////////////////*/
/*     		//définition des différentes variables globales*/
/*     		///////////////////////////////////////////////*/
/*          	var GV={};*/
/*          	*/
/*          	*/
/* 			//indicateur connexion*/
/*          	GV.isConnected;*/
/*          	{% if app.session.get('user')==null %}*/
/*          		GV.isConnected=false;*/
/*                 {% else %}*/
/* 			GV.isConnected=true;*/
/* 		{% endif %}*/
/* */
/* 			*/
/* */
/* 		//url pour gérer l'environnement de prod quand twig n'est pas pleinement accessible*/
/* 		GV.urlPrefix;*/
/* 		GV.mode;*/
/* */
/* 		{% if app.environment=="dev" %}*/
/*                     GV.urlPrefix="/app_dev.php/";*/
/*                     GV.mode="dev";*/
/* 		{% else %}*/
/*                     GV.urlPrefix="/";*/
/*                     GV.mode="prod";*/
/* 		{% endif %}*/
/* */
/* 		//pseudo variable flash pour passer des infos à des faux popups*/
/* 		GV.lastFlDatas;*/
/* */
/*          </script>*/
/*          {% javascripts*/
/* 		 */
/* 	     'js/jquery.js'*/
/*              'bootstrap-3.3.6-dist/js/bootstrap.min.js'*/
/* 	     'js/tsc.js'*/
/*              'js/helpers.js'*/
/*              'libs/gritter/js/jquery.gritter.min.js'*/
/*              'js/buttonWaiter.js'*/
/*              'js/simpleWaiter.js'*/
/*              'js/logic.js' %}*/
/* 		    <script src="{{ asset_url }}"></script>*/
/* 	 {% endjavascripts %}*/
/*         */
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
