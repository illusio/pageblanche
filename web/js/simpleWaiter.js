/** @constructor simpleWaiter*/
function simpleWaiter(_options)
{
var current=this;

////////////////////PROPERTIES/////////////////////////

////////////////////METHODS/////////////////////////
/**
 * mÃ©thode qui fait apparaitre le waiter
 */
 this.show=function(parentDiv)
 {
	$(this.imageElem).appendTo('#'+parentDiv);
        var leftImg=($('#'+parentDiv).width()/2)-($(this.imageElem).width()/2);
        //var topImg=($('#'+parentDiv).height()/2)-($(this.imageElem).height()/2);
        var topImg=20;
         $(this.imageElem).css('left',leftImg+'px');
         $(this.imageElem).css('top',topImg+'px');
 };
 
 /**
 * mÃ©thode qui fait disparaitre le waiter
 */
 this.hide=function()
 {
	$(this.imageElem).remove();
 };
 
 
////////////////////AFFECTATION/////////////////////////
 var options;//objject that contains all the parameters
 if (_options == null) 
 {
 	options=
 	{
		_linkImage:'something',
		_idImage:'something'

 	};
 }
 else
 {
 	options=_options;
 }
 this.linkImage=options._linkImage;
 this.idImage=options._idImage;
 
 this.imageElem=document.createElement('img');
 this.imageElem.setAttribute('src',this.linkImage);
 $(this.imageElem).css('position','absolute');
 
 
 

}