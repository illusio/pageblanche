/** @constructor twoStatesComponent*/

function twoStatesComponent(_options)

{

var current=this;

////////////////////CONSTANTES/////////////////////////


 /**

 * 

 */

 this.LAYOUT_DISPOSITION_HORIZONTAL="horizontal";

 

 /**

 * 

 */

 this.LAYOUT_DISPOSITION_VERTICAL="vertical";

 

 /**

  * 

  */

  this.TRANSITION_BALAYAGE="balayage";

  

  /**

  * 

  */

  this.TRANSITION_FONDUE="fondue";


 /**

  * 

  */

  this.TRANSITION_NONE="none";

////////////////////PROPERTIES/////////////////////////


 /**

 * id of the target div

 */

 this.targetId='something';

 

 

 /**

 * width of the component

 */

 this.width='something';

 

 

 /**

 * height of the component

 */

 this.height='something';

 

 

 /**

 * type of transition when we change of state

 */

 this.transitionType='something';

 

 

 /**

 * string that represent the layout disposition => horizontal or vertival

 */

 this.layoutDisposition='something';

 

 

 /**

 * duration of a transition

 */

 this.transitionDuration='something';

 

 

 /**

 * div that contains the two divs that match to the two states( it is generated )

 */

 this.contDivs='something';

 

 

 /**

 * boolean that inform if we are on the second state

 */

 this.isOn=false;

 

 

////////////////////METHODS/////////////////////////

/**

 * function that makes the layout

 */

 this.make=function()

 {

this.width=$("#"+this.targetId).width();

this.height=$("#"+this.targetId).height();

//$("#"+this.targetId).css("position","relative");

$("#"+this.targetId).css("overflow","hidden");

 

$("#"+this.targetId).children().css("width",this.width+"px");

$("#"+this.targetId).children().css("height",this.height+"px");

$("#"+this.targetId).children().css("margin-left","0px");

$("#"+this.targetId).children().css("margin-top","0px");

var children=$("#"+this.targetId).children().clone();

$("#"+this.targetId).children().remove();

 

this.contDivs=document.createElement("div");

this.contDivs.setAttribute("id",this.targetId+"realCont");

$(this.contDivs).appendTo("#"+this.targetId);

$(this.contDivs).css("position","relative");

children.appendTo("#"+$(this.contDivs).attr("id"));

 

 

if(this.layoutDisposition==this.LAYOUT_DISPOSITION_HORIZONTAL)

{

var brClear=document.createElement("br");

$(brClear).css("clear","both");

$(brClear).appendTo("#"+$(this.contDivs).attr("id"));

children.css("float","left");

$(this.contDivs).css("width",this.width*2+"px");

$(this.contDivs).css("height",this.height+"px");

}


 

 

/*switch(this.layoutDisposition)

{

case this.LAYOUT_DISPOSITION_HORIZONTAL:

break;

 	

case this.LAYOUT_DISPOSITION_HORIZONTAL:

break;

};*/

//$("#"+this.targetId).css("position","relative");

 };

 

 /**

 * function that set the comonent on on

 */

 this.setOn=function()

 {

if(!this.isOn)

{

this.isOn=true;

switch(this.transitionType)

{

case this.TRANSITION_BALAYAGE:

if(this.layoutDisposition==this.LAYOUT_DISPOSITION_HORIZONTAL)

{

$(this.contDivs).animate({marginLeft:-current.width+"px"},{duration:current.transitionDuration});

}

else

{

$(this.contDivs).animate({marginTop:-current.height+"px"},{duration:current.transitionDuration});

}

break;

 	

case this.TRANSITION_FONDUE:

if(this.layoutDisposition==this.LAYOUT_DISPOSITION_HORIZONTAL)

{

$(this.contDivs).css("opacity","0");

$(this.contDivs).css("margin-left",-current.width+"px");

$(this.contDivs).animate({opacity:"1"},{duration:current.transitionDuration});

}

else

{

$(this.contDivs).css("opacity","0");

$(this.contDivs).css("margin-top",-current.height+"px");

$(this.contDivs).animate({opacity:"1"},{duration:current.transitionDuration});

}

break;

case this.TRANSITION_NONE:

if(this.layoutDisposition==this.LAYOUT_DISPOSITION_HORIZONTAL)

{

$(this.contDivs).css("margin-left",-current.width+"px");

}

else

{

$(this.contDivs).css("margin-top",-current.height+"px");

}

break;

};

}

 

 };

 

 /**

 * function that set the comonent on off

 */

 this.setOff=function()

 {

if(this.isOn)

{

this.isOn=false;

switch(this.transitionType)

{

case this.TRANSITION_BALAYAGE:

if(this.layoutDisposition==this.LAYOUT_DISPOSITION_HORIZONTAL)

{

$(this.contDivs).animate({marginLeft:"0px"},{duration:current.transitionDuration});

}

else

{

$(this.contDivs).animate({marginTop:"0px"},{duration:current.transitionDuration});

}

break;

 	

case this.TRANSITION_FONDUE:

if(this.layoutDisposition==this.LAYOUT_DISPOSITION_HORIZONTAL)

{

$(this.contDivs).css("opacity","0");

$(this.contDivs).css("margin-left","0px");

$(this.contDivs).animate({opacity:"1"},{duration:current.transitionDuration});

}

else

{

$(this.contDivs).css("opacity","0");

$(this.contDivs).css("margin-top","0px");

$(this.contDivs).animate({opacity:"1"},{duration:current.transitionDuration});

}

break;

case this.TRANSITION_NONE:

if(this.layoutDisposition==this.LAYOUT_DISPOSITION_HORIZONTAL)

{

$(this.contDivs).css("margin-left","0px");

}

else

{

$(this.contDivs).css("margin-top","0px");

}

break;

};

}

 };

 

 

////////////////////AFFECTATION/////////////////////////

 var options;//objject that contains all the parameters

 if (_options == null) 

 {

 	options=

 	{

_targetId:'something',

_layoutDisposition:'something',

_transitionType:'something',

_transitionDuration:'something'


 	};

 }

 else

 {

 	options=_options;

 }

 this.targetId=options._targetId;

 this.layoutDisposition=options._layoutDisposition;

 this.transitionType=options._transitionType;

 this.transitionDuration=options._transitionDuration;

 

 this.make();


}