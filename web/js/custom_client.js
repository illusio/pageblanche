function init_client()
{
    $("#sendBt").click(function(event)
     {
         $('.myField2').removeClass('errorClass');
        
         GV.buttonWaiter.showWaiting("#sendBt","/images/layout/1-1.gif");
                        $.ajax({
			type: 'POST',
			url: GV.urlPrefix+"test_client",
			data:{
				login:$("#login").val(),
				password:$("#password").val(),
				url:$("#url").val(),
				uri:$("#uri").val(),
				methode:$("#methode").val(),
				params:$("#params").val()
				},
			success: function(retour)
			{ 
                                GV.buttonWaiter.unshow();
				if(retour.result=="ok")
                                {
                                    //$('.myField2').val('');
                                    gritterShow("Succès",retour.message,"echecGritter");
                                    
                                }
                                else
                                {
                                     gritterShow("Echec",retour.message,"echecGritter");
                                    if(retour.errors)
                                    {
					
					for(var erreur in retour.errors)
					{
                                            if(retour.errors[erreur]['isValid']==false)
                                            {
                                                $('#'+erreur).addClass("errorClass");
                                            }
                                           
                                            
                                        }
                                    }
                                }
				
				
			},
			dataType: 'json'
                        });
     });
}


