function init_authent()
{
    $("#connexionBt").click(function(event)
     {
         $('.myField2').removeClass('errorClass');
        
         GV.buttonWaiter.showWaiting("#connexionBt","/images/layout/1-1.gif");
                        $.ajax({
			type: 'POST',
			url: GV.urlPrefix+"connect_user",
			data:{email:$("#emailToConnect").val(),password:$("#mdpToConnect").val()},
			success: function(retour)
			{ 
                                GV.buttonWaiter.unshow();
				if(retour.result=="ok")
                                {
                                    $('.myField2').val('');
                                    document.location.href=GV.urlPrefix;
                                    
                                }
                                else
                                {
                                     gritterShow("Echec",retour.message,"echecGritter");
                                    if(retour.errors)
                                    {
					
					for(var erreur in retour.errors)
					{
                                            if(retour.errors[erreur]['isValid']==false)
                                            {
                                                $('#'+erreur).addClass("errorClass");
                                            }
                                           
                                            
                                        }
                                    }
                                }
				
				
			},
			dataType: 'json'
                        });
     });
}


