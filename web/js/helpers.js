/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getTimeFromFormatedDate(format,sepa,value)
{
	var arrayDatas=value.split(sepa);
	var arrayFormat=format.split(sepa);
	var day;
	var month;
	var year;
	
	if(arrayFormat.length!=3 || arrayDatas.length!=3)
	{
		return false;
	}
	
	for(var i=0;i<arrayFormat.length;i++)
	{
		if(arrayFormat[i]=="dd"){day=arrayDatas[i];}
		if(arrayFormat[i]=="mm"){month=arrayDatas[i];}
		if(arrayFormat[i]=="yy"){year=arrayDatas[i];}
	}
        
	var theDate=new Date();
	theDate.setFullYear(year);
        theDate.setDate(day);
        theDate.setMonth(month-1);
        return Math.round(theDate.getTime()/1000);

}

function gritterShow(titre,message,cssClass)
{
    var unique_id = $.gritter.add({
                                    // (string | mandatory) the heading of the notification
                                    title: titre,
                                    // (string | mandatory) the text inside the notification
                                    text: message,

                                    // (bool | optional) if you want it to fade out on its own or just sit there
                                    sticky: false,
                                    // (int | optional) the time you want it to be alive for before fading out
                                    time: '',
                                    // (string | optional) the class name you want to apply to that specific message
                                    class_name: 'my-sticky-class '+cssClass
                            });
}


