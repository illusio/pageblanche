<?php
//

include ("src/baseClasseContent.php");//import of the main content that do all the job
include ("src/baseGlobalInfosContent.php");//import of the main content that do the job for the global infos
include ("src/functions.php");//import of some functions 
include ("src/realExporter.php");//import of the ... real exporter
//include("src/globalInfos.php");//a class that can stock anything

//admin path from the document root with only separator slashes
$adminPath="orm/nacarat";


//name of the database that is used to generate the backend
$base = "nacarat";

//boolean that defines if it's a simple backend
$generationSimple = true;

//database infos
$infos = 'mysql:host=localhost;dbname=' . $base . '';
$user = "nacarat";
$mdp = "xk3mgg";
$dbh = new PDO($infos, $user, $mdp, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));



//simples instructions for the directories and the files to copy
mkdir($base);
mkdir($base . "/libs");
mkdir($base . "/model");
mkdir($base . "/libs/php");
mkdir($base . "/tmp");
mkdir($base . "/callbacks");
mkdir($base . "/config");
copy("src/formCheck.php", $base . "/libs/php/formCheck.php");
copy("src/imgSaver.php", $base . "/libs/php/imgSaver.php");
copy("src/myTools.php", $base . "/libs/php/myTools.php");
copy("src/baseModel.php", $base . "/libs/php/baseModel.php");
copy_dir("src/js/", $base . "/libs/js/");
copy("src/index.php", $base . "/index.php");
copy("src/auth.php", $base . "/auth.php");
copy("src/router.php", $base . "/router.php");
copy("src/disconnect.php", $base . "/disconnect.php");
copy_dir("src/bootstrap/", $base . "/libs/bootstrap/");
copy_dir("src/css/", $base . "/css/");
copy_dir("src/components/", $base . "/components/");
copy_dir("src/services/", $base . "/services/");
copy_dir("src/medias/", $base . "/medias/");

//we launch the export function
export($base,$generationSimple,$infos,$user,$mdp,$dbh);

//we create the global infos class
$globalInfosClass=getGlobalInfos($adminPath,$base,$infos,$user,$mdp);
file_put_contents($base."/libs/php/globalInfos.php",$globalInfosClass);




?>