<?php
// src/Acme/SoapBundle/Services/HelloService.php
namespace AppBundle\Helpers;

use AppBundle\Helpers\UtilClass;

class MySoapServer
{
    public $ormManager;
    public function __construct() {
        // Throw SOAP fault for invalid username and password combo
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) ) {
            throw new SOAPFault("Paramètres d'authentification incorrects", 401);
        }
		
		if ($_SERVER['PHP_AUTH_USER']!="userWB" || $_SERVER['PHP_AUTH_PW']!="asde77_u" ) {
            throw new SOAPFault("Paramètres d'authentification incorrects", 401);
        }
    }
    
    public function changeState($ref_lot,$etat)
    {
        
        require_once "orm/nacarat/libs/php/globalInfos.php";
		$this->ormManager=new \globalInfos(UtilClass::rewritingOrNot());
        $this->ormManager->requireModel('lots');
        $this->ormManager->requireModel('etats');
        
        $reqLot=$this->ormManager->doQuery("lots","*","WHERE lots.lot_ref=:lotRef",array(":lotRef"=>$ref_lot));
        if(count($reqLot)==0)
        {
            return "Lot introuvable";
        }
        
        $reqEtat=$this->ormManager->doQuery("etats","*","WHERE etats.idetats=:etat",array(':etat'=>$etat));
        if(count($reqEtat)==0)
        {
            return "Etat inexistant";
        }
        
        $lot=new \lots(UtilClass::rewritingOrNot());
        
        $lot->initFromDatas(array('idlots'=>$reqLot[0]['idlots']));
        
		if($etat!=1)
		{
			$lot->set('lot_is_option','0');
			
		}
		else
		{
			$lot->set('lot_is_option',1);
		}
        $lot->set('etats_idetats',$etat);
        
        if($lot->save())
        {
            return "Etat du lot actualisé à ".$reqEtat[0]['etat_label'];
        }
        else
        {
            return "Actualisation de l'état du lot impossible pour l'instant";
        }
        
    }
}