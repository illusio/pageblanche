<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\MyBaseController;
use AppBundle\Helpers\UtilClass;
use AppBundle\Helpers\CustomQueries;
use AppBundle\Helpers\StateServer;
use AppBundle\Helpers\MySoapServer;

class DefaultController extends MyBaseController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        
        $programmes=$this->ormManager->doQuery("programmes","*");
        
        for($i=0;$i<count($programmes);$i++)
        {
            $anneeLivraison=substr($programmes[$i]['prog_date_livraison'],0,4);
            $moisLivraison=  str_replace($anneeLivraison, "", $programmes[$i]['prog_date_livraison']);
            $programmes[$i]['livraisonFormat']=$moisLivraison."<sup>".($moisLivraison==1?"er":"eme")."</sup> trimestre ".$anneeLivraison;
            $programmes[$i]['hqe']=($programmes[$i]['prog_is_hqe']==1?"oui":"non");
            $programmes[$i]['bbc']=($programmes[$i]['prog_is_bbc']==1?"oui":"non");
        }
       
        return $this->render('default/accueil.html.twig', array(
            "programmes"=>$programmes,
            "userInfos"=>$this->userInfos
        ));
    }
    
     /**
     * @Route("/authentification", name="authentification")
     */
    public function authentAction(Request $request)
    {
        
        
        //place your script here
       
        return $this->render('default/authentification.html.twig', array(
            
        ));
    }
    
    /**
     * @Route("/liste_lots/{idprog}", name="liste_lots")
     */
    public function lotsAction($idprog)
    {

        $prog=$this->ormManager->doQuery("programmes","*","WHERE programmes.idprogrammes=:idprog",array(':idprog'=>$idprog));
        $prog=$prog[0];
        $lots=$this->ormManager->doQuery("lots","*","LEFT JOIN etats ON etats.idetats=lots.etats_idetats WHERE lots.programmes_idprogrammes=:idprog",array(':idprog'=>$idprog));
        for($i=0;$i<count($lots);$i++)
        {
            $anneeLivraison=substr($lots[$i]['lot_date_livraison'],0,4);
            $moisLivraison=  str_replace($anneeLivraison, "", $lots[$i]['lot_date_livraison']);
            $lots[$i]['livraisonFormat']=$moisLivraison."<sup>".($moisLivraison==1?"er":"eme")."</sup> trimestre ".$anneeLivraison;
            $lots[$i]['tvaRed']=($lots[$i]['lot_is_tva_reduite']==1?"oui":"non");
            $lots[$i]['isOption']=($lots[$i]['lot_is_option']==1?"oui":"non");
        }
        
       
        return $this->render('default/lots.html.twig', array(
            "lots"=>$lots,"prog"=>$prog,
            "userInfos"=>$this->userInfos
        ));
    }
    
        /**
     * @Route("/liste_denonces/{idprog}", name="liste_denonces")
     */
    public function denoncesAction($idprog)
    {

        $prog=$this->ormManager->doQuery("programmes","*","WHERE programmes.idprogrammes=:idprog",array(':idprog'=>$idprog));
        $prog=$prog[0];
        $denonces=$this->ormManager->doQuery("denonciations","*","WHERE denonciations.programmes_idprogrammes=:idprog",array(':idprog'=>$idprog));
        
        
       
        return $this->render('default/denonces.html.twig', array(
            "denonces"=>$denonces,"prog"=>$prog,
            "userInfos"=>$this->userInfos
        ));
    }
    
    
    

    
    
    
 
    

    
     
}



