<?php

namespace AppBundle\Controller;

use AppBundle\Helpers\LangClass;
use AppBundle\Helpers\UtilClass;
use AppBundle\Helpers\CustomQueries;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MyBaseController extends Controller
{
	
	public $ormManager;
	
	public $userInfos=false;
        private $routesPrives=array
        (
            "homepage","liste_lots"
        );
	
	
	
	//fonction exécutée avant chaque appel de méthode de type controller
	public function preExecute()
	{
                //exit;
		//on récupère l'orm
		require_once "orm/nacarat/libs/php/globalInfos.php";
		$this->ormManager=new \globalInfos(UtilClass::rewritingOrNot());

		// on récupère la session
		$session = $this->getRequest()->getSession();

		$this->userInfos=false;
		//$this->devConnect();
		//si on a un user connecté, on vérifie qui est il et on on  rempli une variable global avec les données
		if($session->has('user'))
		{
                    
			$getUserInfos=CustomQueries::getUserInfos($this->ormManager, $session->get("user"));
			if(!$getUserInfos)
			{
				$this->userInfos=false;
			}
			else
			{
				$this->userInfos=$getUserInfos;
			}
		}
                if(in_array($this->container->get('request')->get('_route'),$this->routesPrives))
                {
                    if(!$this->userInfos)
                    {
                        $this->redirectToLogin();
                    }
                }
               

	}
	
	public function devConnect()
	{
		//$session = $this->getRequest()->getSession();
		//$session->set('user',2);
	}
	
	public function disconnectUser() 
	{
		/*$this->userInfos=false;
		$session = $this->getRequest()->getSession();
		$session->remove('user');
		$session->remove('facebookId');*/
	}
        
        public function redirectToLogin()
        {
        $route=$this->generateUrl('authentification');
        header('Location: '.$route.''); exit; 
        }
	
	
	
	
	
    
    
}
