<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\MyBaseController;
use AppBundle\Helpers\UtilClass;
use AppBundle\Helpers\CustomQueries;
use AppBundle\Helpers\CustomSoap;
use AppBundle\Helpers\DateServer;
use AppBundle\Helpers\Formcheck;

class SoapController extends MyBaseController
{
    
    //private $hasPort=":8888";
    private $hasPort="";
     /**
     * @Route("/soapserver", name="soapserver")
     */
    public function soapServer0Action(Request $request)
    {
        
        $params=array('uri'=>"http://".$this->getRequest()->getHost().$this->hasPort."/app_dev.php/soapserver");
        $server=new \SoapServer(NULL,$params);
        
        //$server->setClass('MySoapServer');
        $server->setObject($this->get('ss_service'));
        $server->handle();
       exit;
    }
    
	
	 /**
     * @Route("/test_entrant", name="test_entrant")
     */
    public function testOursAction(Request $request)
    {
    	$params=array
    	(
    		'location'=>"http://137.74.47.233/app_dev.php/soapserver",
    		'uri'=>"urn://137.74.47.233/app_dev.php/soapserver",
    		'trace'=>true,
    		'login'=>"userWB",
    		'password'=>"asde77_u"
		);
    	 $client= new CustomSoap(NULL,$params);
		 echo $client->changeState("1014356",1);
		var_dump($client->__getLastRequest());
		 exit;
	}
	
	/**
     * @Route("/test_client", name="test_client")
     */
    public function testClientAction(Request $request)
    {
    	
		//vérification des données
        $checker=new Formcheck();
        $checker->addCheck("login",$_POST['login'],"simple");
        $checker->addCheck("password",$_POST['password'],"simple");
		$checker->addCheck("url",$_POST['url'],"simple");
		$checker->addCheck("uri",$_POST['uri'],"simple");
		$checker->addCheck("methode",$_POST['methode'],"simple");
		$checker->addCheck("params",$_POST['params'],"simple");
        if(!$checker->isValid())
    	{
            echo json_encode(array('result'=>"ko",'message'=>"Vérifiez d'avoir bien rempli tous les champs","errors"=>$checker->getResults()));exit;
    	}
    	//exit;
    	$params=array
    	(
    		'location'=>$_POST['url'],
    		'uri'=>$_POST['uri'],
    		'trace'=>true,
    		'login'=>$_POST['login'],
    		'password'=>$_POST['password']
		);
		
    	 //$client= new CustomSoap(NULL,$params);
    	 $client=new \SoapClient(NULL,$params);
		 $theParams=explode(',',$_POST['params']);
		 
		 $retour=$client->__soapCall($_POST['methode'],array($theParams[0],$theParams[1]));
		 //$retour=$client->changeState("1014356",1);
		 echo json_encode(array('result'=>"ok",'message'=>$retour));exit;
		
	}
	
	/**
     * @Route("/custom_client", name="custom_client")
     */
    public function customClientAction(Request $request)
    {
    	return $this->render('default/custom_client.html.twig', array(
           
        ));
	}
     /**
     * @Route("/soapclient", name="soapclient")
     */
    public function soapClient0Action(Request $request)
    {
        // phpinfo();exit;
      	//point d'entrée
		//echo file_get_contents("https://nacpartnersws-rec.rabotdutilleul.com/XrmImportDataService");	exit;
		try {
			
			//CEtte partie permet de passer outre le certificat
			/*$context = stream_context_create([
			    'ssl' => [
			        // set some SSL/TLS specific options
			        'verify_peer' => false,
			        'verify_peer_name' => false,
			        'allow_self_signed' => true
			    ]
			]);*/
			$localCert="/var/www/20150428_Wildcard_RabotDutilleul_Com.pem";
			$passphrase="ssl247";
		    $client= new \SoapClient("https://nacpartnersws-rec.rabotdutilleul.com/XrmImportDataService?singleWsdl",
        							array(
        									'login'=> "illusioporrec@nacarat.com",
                                            'password'=> "Illusio@2016",
                                            'local_cert'=>$localCert,
                                            'passphrase'=>$passphrase
                                            //'stream_context' => $context//permet de passer outre le certificat
									     ));
										 
										 
			//essai d'ajout de header mais çà donne le même résultat sans cette partie 							 
			$username = "illusioporrec@nacarat.com";
			$password = "Illusio@2016";
			
			
			/*$headerbody = array('username ' => $username, 
			                     'password ' => $password); 
			$ns = 'http://www.cimail.fr/CimailXrm/WebServices'; //Namespace of the web service.
			$header = new \SOAPHeader($ns, 'requestHeader', $headerbody);
			$client->__setSoapHeaders($header);*/
			
				
			////////////////////
			
			
			//appel qui soulève le message d'erreur could not connect to host			 
			$client->__soapCall('PushData',array());exit;
			
			//appel basique qui liste les méthodes du webservice
			$taballservices=$client->__getFunctions();
			echo "<pre>";var_dump($taballservices);exit;
			
			
		} catch (SoapFault $e) {
			var_dump($e);exit;
		    echo 'Exception reçue : ',  $e->getMessage(), "\n";
		}
		
		
    }
}
