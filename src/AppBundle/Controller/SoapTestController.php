<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\MyBaseController;
use AppBundle\Helpers\UtilClass;
use AppBundle\Helpers\Formcheck;

class SoapTestController extends MyBaseController
{

    
    /**
     * @Route("/soap_server2", name="soap_server2")
     */
    public function soapServer2Action(Request $request)
    {
        require_once "nusoap-0.9.5/lib/nusoap.php";
         $serv= new \nusoap_server();

        $serv->configureWSDL("Hello", "urn:Hello/", "http://pageblanche:8888/app_dev.php/soap_server2");

        $serv->wsdl->schemaTargetNamespace="http://soapinterop.org/xsd/";

        $serv->register("bonjour",
          array("nom"=>"xsd:string", "prenom"=>"xsd:string"),
          array("codret"=>"xsd:string"),
          'urn:Hello',
          'urn:Hello/bonjour',
          '',
          '',
          'WebService pour dire bonjour...');

        $serv->service(isset($HTTP_RAW_POST_DATA)?$HTTP_RAW_POST_DATA:'');

        function bonjour($nom, $prenom)

        {
           return 'Bonjour '.$prenom.' '.$nom;

        }
        exit;
    }
    /**
     * @Route("/soap_client2", name="soap_client2")
     */
    public function soapClient2Action(Request $request)
    {
        require_once "nusoap-0.9.5/lib/nusoap.php";
        $client = new \nusoap_client("http://pageblanche:8888/app_dev.php/soap_server2?wsdl", true);
        $err= $client->getError();
        if($err) die('erreur de connexion au service: '.$err);
        var_dump($client->call("bonjour", array("nom"=>"toto", "prenom"=>"tata")));
        $err= $client->getError();
        if($err) die('erreur d\'execution du service: '.$err);
        exit;
    }
    /**
     * @Route("/soap_server3", name="soap_server3")
     */
    public function soapSever3(Request $request)
    {
        $server = new \SoapServer('http://pageblanche:8888/hello.wsdl');
        $server->setObject($this->get('hello_service'));

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $server->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }
    
        /**
     * @Route("/soap_client3", name="soap_client3")
     */
    public function soapClient3(Request $request)
    {
        $client = new \Soapclient('http://pageblanche:8888/hello.wsdl');

        $result = $client->call('hello', array('name' => 'Scott'));

        return $response;
    }
    
    public function getProd()
    {
        return "voilà les produits";
    }
    
    
    
    /**
     * @Route("/soap_server", name="soap_server")
     */
    public function soapServerAction(Request $request)
    {
        
        require_once "nusoap-0.9.5/lib/nusoap.php";
        $server = new \soap_server();
        $server->configureWSDL("foodservice", "http://pageblanche:8888/app_dev.php/foodservice");

        $server->register("getFood",
            array("type" => "xsd:string"),
            array("return" => "xsd:string"),
            "http://pageblanche:8888/foodservice",
            "http://pageblanche:8888/foodservice#getFood",
            "rpc",
            "encoded",
            "Get food by type");
        $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        @$server->service(file_get_contents("php://input"));
        exit;
    }
    
     /**
     * @Route("/soap_client", name="soap_client")
     */
    public function soapClientAction(Request $request)
    {
        require_once "nusoap-0.9.5/lib/nusoap.php";
        $client = new \nusoap_client("http://pageblanche:8888/app_dev.php/soap_server?wsdl", true);
        $error  = $client->getError();
       

        if ($error) {
            echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
        }

        $result = $client->call("getFood", array("type" => "Main"));

        if ($client->fault) {
            echo "<h2>Fault</h2><pre>";
            print_r($result);
            echo "</pre>";
        } else {
            $error = $client->getError();
            if ($error) {
                echo "<h2>Error</h2><pre>" . $error . "</pre>";
            } else {
                echo "<h2>Main</h2>";
                echo $result;
            }
        }
         exit;
        // show soap request and response
        echo "<h2>Request</h2>";
        echo "<pre>" . htmlspecialchars($client->request, ENT_QUOTES) . "</pre>";
        echo "<h2>Response</h2>";
        echo "<pre>" . htmlspecialchars($client->response, ENT_QUOTES) . "</pre>";
        exit;
    }
    
    
function getFood($type) {
            switch ($type) {
                case 'starter':
                    return 'Soup';
                    break;
                case 'Main':
                    return 'Curry';
                    break;
                case 'Desert':
                    return 'Ice Cream';
                    break;
                default:
                    break;
            }
        }
}
