<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\MyBaseController;
use AppBundle\Helpers\UtilClass;
use AppBundle\Helpers\Formcheck;

class ServicesController extends MyBaseController
{

     /**
     * @Route("/import_xml", name="import_xml")
     */
    public function importAction(Request $request)
    {
        // replace this example code with whatever you need
        /*return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));*/
        $theXml=simplexml_load_file("xmls/NACARAT.xml");
        $this->ormManager->requireModel('programmes');
        $this->ormManager->requireModel('lots');
        $this->ormManager->requireModel('annexes');
        //var_dump($theXml->reponse);exit;
        foreach($theXml as $node)
        {
            //echo "huhu <br/>";
            //echo $node->getName()."<br/>";
            //echo $node->REF_OPERATION;
            if($node->getName()=="PROGRAMME")
            {
                $boolNewProg=false;
                $aProg=new \programmes(UtilClass::rewritingOrNot());
                if(!$aProg->initFromDatas(array('prog_ref'=>$node->REF_OPERATION)))
                {
                    $boolNewProg=true;
                    $aProg->set('prog_ref',$node->REF_OPERATION);
                    $aProg->set('prog_numero',$node->NUMERO);
                }
                /*else
                {
                    
                }*/
                
                $aProg->set('prog_nom',$node->NOM);
                $aProg->set('prog_adresse',$node->ADRESSE);
                $aProg->set('prog_cp',$node->CP);
                $aProg->set('prog_ville',$node->VILLE);
                $aProg->set('prog_mail',$node->ADRESSE_MAIL);
                $aProg->set('prog_url',$node->ADRESSE_WEB);
                $aProg->set('prog_image_url',$node->PERSPECTIVE);
                $aProg->set('prog_plaquette_url',$node->PLAQUETTE);
                //$aProg->set('prog_nom',$node->TYPE_BIEN);
                $aProg->set('prog_nb_lots',$node->NB_LOTS);
                $aProg->set('prog_date_livraison',$node->DATE_LIVRAISON);
                $aProg->set('prog_date_vente',$node->DATE_VENTE);
                $aProg->set('prog_lat',$node->LATITUDE);
                $aProg->set('prog_lng',$node->LONGITUDE);
                //$aProg->set('prog_is_bbc',($node->BBC=="N"?"0":1);
                $aProg->set('prog_is_bbc',($node->BBC=="N"?"0":1));
                $aProg->set('prog_is_bbc',($node->BBC=="N"?"0":1));
                $aProg->set('prog_descriptif_cours',$node->DESCRIPTIF_COURT);
                $aProg->set('prog_descriptif_long',$node->DESCRIPTIF_LONG);
                $aProg->set('prog_descriptif_cours_en',$node->DESCRIPTIF_COURT_EN);
                $aProg->set('prog_descriptif_long_en',$node->DESCRIPTIF_LONG_EN);
                
                if($aProg->save())
                {
                    if($boolNewProg)
                    {
                        echo "Nouveau programme ".$node->NOM." enregistré <br/>";
                    }
                    else
                    {
                        echo "Programme ".$node->NOM." mis à jour <br/>";
                    }
                }
                echo "<br/>";
                foreach($node as $sousNode)
                {
                    
                    if($sousNode->getName()=="LOT")
                    {
                        
                        
                        
                        $aLot=new \lots(UtilClass::rewritingOrNot());
                        $boolNewLot=false;
                        if(!$aLot->initFromDatas(array("lot_ref"=>$sousNode->REF_LOT,"programmes_idprogrammes"=>$aProg->get('idprogrammes'))))
                        {
                            $boolNewLot=true;
                            $aLot->set('lot_ref',$sousNode->REF_LOT);
                            $aLot->set('lot_numero',$sousNode->NUMERO);
                        }
                        
                        $aLot->set('lot_url',$sousNode->ADRESSE_WEB);
                        $aLot->set('lot_type_bien',$sousNode->TYPE_BIEN);
                        $aLot->set('lot_orientation',$sousNode->ORIENTATION);
                        $aLot->set('lot_surface_habitable',$sousNode->SURFACE_HABITABLE);
                        //$aLot->set('lot_surface_terrain',$sousNode->ADRESSE_WEB);
                        $aLot->set('lot_nb_piece',$sousNode->NB_PIECES);
                        $aLot->set('lot_etage',$sousNode->ETAGE);
                        $aLot->set('lot_date_livraison',$sousNode->DATE_LIVRAISON);
                        $aLot->set('lot_date_vente',$sousNode->DATE_VENTE);
                        $aLot->set('lot_prix_accession',$sousNode->PRIX_ACCESSION);
                        $aLot->set('lot_loyer',$sousNode->LOYER);
                        $aLot->set('lot_is_tva_reduite',($sousNode->TVA_REDUITE=="N"?"0":1));
                        $aLot->set('lot_is_option',($sousNode->OPTION!=1?"0":1));
                        $aLot->set('etats_idetats',($sousNode->OPTION!=1?3:1));
                        $aLot->set('lot_taux_tva',$sousNode->TAUX_TVA);
                        $optionFin=$this->convertOptionFinToTimestamp($sousNode->OPTION_FIN);
                        if($optionFin)
                        {
                            $aLot->set('lot_option_fin',$optionFin);
                        }
                        $aLot->set('lot_plan_vente_url',$sousNode->PLAN_VENTE);
                        $aLot->set('lot_nb_parkings',$sousNode->PARKINGS);
                        $aLot->set('lot_url',$sousNode->GARAGES);
                        //$aLot->set('lot_url',$sousNode->CAVE);
                        $aLot->set('lot_num_tranche',$sousNode->NUM_TRANCHE);
                        $aLot->set('lot_nom_batiment',$sousNode->NOM_BATIMENT);
                        $aLot->set('programmes_idprogrammes',$aProg->get('idprogrammes'));
                        //$aLot->set('lot_url',$sousNode->ADRESSE_WEB);
                        //$aLot->set('lot_url',$sousNode->ADRESSE_WEB);
                        
                        if($aLot->save())
                        {
                            if($boolNewLot)
                            {
                                echo "Nouveau lot ".$sousNode->NUMERO." enregistré <br/>";
                            }
                            else
                            {
                                echo "Lot ".$sousNode->NUMERO." mis à jour <br/>";
                            }
                        }
                        
                        echo "<br/>";
                        foreach($sousNode as $lastNode)
                        {
                            if($lastNode->getName()=="LOT_SECOND")
                            {
                                echo $lastNode['LS_NUMERO'];
                                $oneAnn=new \annexes(UtilClass::rewritingOrNot());
                                $boolNewAnnexe=false;
                                if(!$oneAnn->initFromDatas(array("annexe_numero"=>$lastNode->LS_NUMERO,"lots_idlots"=>$aLot->get('idlots'))))
                                {
                                    $boolNewAnnexe=true;
                                    $oneAnn->set('annexe_numero',$lastNode->LS_NUMERO);
                                    
                                }
                                
                                $oneAnn->set('annexe_type',$lastNode->LS_TYPE);
                                $oneAnn->set('annexe_prix',$lastNode->LS_PRIX);
                                $oneAnn->set('lots_idlots',$aLot->get('idlots'));
                                if($oneAnn->save())
                                {
                                    if($boolNewAnnexe)
                                    {
                                        echo "Nouvelle annexe ".$lastNode->LS_NUMERO." enregistrée <br/>";
                                    }
                                    else
                                    {
                                        echo "Annexe ".$lastNode->LS_NUMERO." mise à jour <br/>";
                                    }
                                }
                                echo "<br/>";
                            }
                        }
                    }
                }
                
                //exit;
                echo "<br/><br/><br/>";
                
            }
        }
        exit;
        echo "huhu";exit;
        
    }

     /**
     * @Route("/create_denonce", name="create_denonce")
     */
    public function createDenonceAction(Request $request)
    {
        if(!$this->userInfos)
    	{
            echo json_encode(array('result'=>"ko",'message'=>"Vous êtes actuellement connecté"));exit;
    	}
        //vérification des données
        $checker=new Formcheck();
        $checker->addCheck("contact_nom",$_POST['contact_nom'],"simple");
        $checker->addCheck("contact_ville",$_POST['contact_ville'],"simple");
        $checker->addCheck("contact_prenom",$_POST['contact_prenom'],"simple");
        $checker->addCheck("contact_zipcode",$_POST['contact_zipcode'],"nombre");
        $checker->addCheck("contact_pays",$_POST['contact_pays'],"simple");
        $checker->addCheck("contact_mail",$_POST['contact_mail'],"mail");
        $checker->addCheck("contact_phone",$_POST['contact_phone'],"facultatif");
        $checker->addCheck("contact_commentaire",$_POST['contact_commentaire'],"facultatif");
        if(!$checker->isValid())
    	{
            echo json_encode(array('result'=>"ko",'message'=>"Vérifiez d'avoir bien rempli tous les champs","errors"=>$checker->getResults()));exit;
    	}
        
       
        
        
        
        echo json_encode(array('result'=>"ok",'message'=>""));exit;
    }
    
     /**
     * @Route("/connect_user", name="connect_user")
     */
    public function connectUserAction(Request $request)
    {
        if($this->userInfos)
    	{
            echo json_encode(array('result'=>"ko",'message'=>"Vous êtes actuellement connecté"));exit;
    	}
        //vérification des données
        $checker=new Formcheck();
        $checker->addCheck("emailToConnect",$_POST['email'],"mail");
        $checker->addCheck("mdpToConnect",$_POST['password'],"simple");
        if(!$checker->isValid())
    	{
            echo json_encode(array('result'=>"ko",'message'=>"Vérifiez d'avoir bien rempli tous les champs","errors"=>$checker->getResults()));exit;
    	}
        
        $this->ormManager->requireModel('users');
        $myUser=new \users(UtilClass::rewritingOrNot());
        if(!$myUser->initFromDatas(array('user_mail'=>$_POST['email'])))
        {
           echo json_encode(array('result'=>"ko",'message'=>"L'adresse email saisie ne correspond à aucun compte"));exit; 
        }
        
        if(md5($_POST['password'])!=$myUser->get('user_mdp'))
        {
           echo json_encode(array('result'=>"ko",'message'=>"Le mot de passe indiqué est faux"));exit; 
        }
        
        
        
        $session = $this->getRequest()->getSession();
        $session->set('user',$myUser->get('idusers'));
        
        
        
        echo json_encode(array('result'=>"ok",'message'=>""));exit;
    }
    
    
    
    private function convertOptionFinToTimestamp($val)
    {
        
        if(strlen($val)>0)
        {
            
            $exp1=explode(' ',$val);
            $exp2=explode('/',$exp1[0]);
            
            $exp3=explode(':',$exp1[1]);
            
            return mktime($exp3[0],$exp3[2],0,$exp2[1],$exp2[0],$exp2[2]);
        }
        else
        {
            return false;
        }
        
        
    }

}
